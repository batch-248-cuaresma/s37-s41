const express = require("express");
const router = express.Router();

const userController = require("../controllers/userControllers")

const auth = require("../auth");
const { verify } = require("jsonwebtoken");

//Route for checking if the user's email already exists in the db
//invoke the checkEmailExists function from the controller file later to communicate with our db
//passes the "body" property of our "request" object to the corresponding controller function
router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromUserController=>res.send(resultFromUserController));
})


//Route for user registration
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromUserController=>res.send(resultFromUserController))
});


//Route for user authentication
router.post("/login",(req,res)=>{
	userController.loginUser(req.body).then(resultFromUserController=>res.send(resultFromUserController))
});


//Route for retrieving user details
router.get("/details",auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId:userData.id}).then(resultFromUserController=>res.send(resultFromUserController));

})

//Route to enroll user to a course


router.post("/enroll",(req,res)=>{

	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(resultFromController=>res.send(resultFromController));
})

router.post("/enrollCourse",auth.verify,(req,res)=>{
	let userData = auth.decode(req.headers.authorization);
	let data = {
		userId: userData,
		courseId: req.body.courseId
	}
	userController.enrollCourse(data).then(resultFromController=>res.send(resultFromController));
})

//Allows us to export the router object that will be accessed in our "index.js"
module.exports = router;
