const express = require ("express");
const router = express.Router();

const courseController = require("../controllers/courseControllers")

const auth = require("../auth");

//Route for creating a course

router.post("/",auth.verify,(req,res)=>{

	//data.course.name
	//data.isAdmin

	const data = {
		course : req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

 	courseController.addCourse(data).then(resultFromCourseController =>res.send(resultFromCourseController))
})


//Route for retrieving all the courses

router.get("/all",(req,res)=>{

	courseController.getAllCourses().then(resultFromController=>res.send(resultFromController))

});

//Route for retrieving all the Active courses

router.get("/",(req,res)=>{
	courseController.getAllActive().then(resultFromController=>res.send(resultFromController))
});

//Route for Updating a course
//jwt verification is needed for this route to ensour that the user is logged in before updating a course
router.put("/:courseId",auth.verify,(req,res)=>{
	courseController.updateCourse(req.params, req.body).then(resultFromController=>res.send(resultFromController));
});

//Activity S40
//Archive a course
//In managin a db, it is a common practice to soft delete our records and what we would implement in the "delete" operation of our application

router.patch("/:courseId/archive",auth.verify,(req,res)=>{
	courseController.archiveCourse(req.params, req.body).then(resultFromController=>res.send(resultFromController));
})
//Stretch Goal
router.patch("/:courseId/unarchive",auth.verify,(req,res)=>{
	courseController.unarchiveCourse(req.params, req.body).then(resultFromController=>res.send(resultFromController));
})
//allow us to export the "router" object that will be
module.exports = router;