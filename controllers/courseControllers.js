const Course = require("../models/Course");

// module.exports.addCourse = (reqBody) =>{

// 	let newCourse = new Course({

// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price

// 	});

// 	return newCourse.save().then((course,error)=>{

// 		if(error){
// 			return false;
// 		}else{
// 			return true;
// 		}

// 	})


// }


module.exports.addCourse = (data) =>{

	if(data.isAdmin){
		let newCourse = new Course({

		name: data.course.name,
		description: data.course.description,
		price: data.course.price

		});
		return newCourse.save().then((course,error)=>{
			if(error){
				return false;
			}else{
				return true;
			}
		})

	}else{

		return false;

	}
	

};


//Retrieve all courses
	/*
		Step:
		1.Retrieve all the courses from the database
	*/

module.exports.getAllCourses = ()=>{
	return Course.find({}).then(result=>{
		return result;
	});
};

//Retrieve all Active Courses
/*
	Step:
	1. Retrieve all the course from the db with thew property "isActive" with the property of true;
*/

module.exports.getAllActive = ()=>{
	return Course.find({isActive: true}).then(result=>{
		return result;
	});
};

//Update a course
/*
	Steps:
	1. Create a variable "updatedCourse" which will contain information retrieved from the reqBody
	2. Find and update the course using the courseId retrieved from the reqParams property and the variable "updatedCourse" containing the information from the reqBody

*/

module.exports.updateCourse = (reqParams,reqBody) =>{

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	//findByIdUpdates(document ID, UdatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course,error)=>{

		if(error){
			return false;
		}else{
			return true;
		};
	});
};

//Activity S40

//Archive a course
/*
	Step:
	1.Create a variables "courseStatus" which will contain information rerieved from the reqBody
	2. Find and update the course using the courseId retrieved from the reqParams property and the variable "courseStatus" containing the information from the reqBody
*/

module.exports.archiveCourse = (reqParams,reqBody) =>{

	let courseStatus = {
		isActive : reqBody.isActive
	}
	return Course.findByIdAndUpdate(reqParams.courseId, courseStatus).then((course,error)=>{

		if(error){
			return false;
		}else{
			console.log(reqBody)
			return true;
		};
	});
}

//Unarchive a Course
/*
	Steps:
	1.Create a variables "reqBody" which will contain information rerieved from the reqBody
	2. Find and update the course using the courseId retrieved from the reqParams property and the variable "reqBody" containing the information from the reqBody
*/

module.exports.unarchiveCourse = (reqParams,reqBody) =>{
	reqBody.isActive = true;
	return Course.findByIdAndUpdate(reqParams.courseId, reqBody).then((course,error)=>{

		if(error){
			return false;
		}else{
			console.log(reqBody)
			return true;
		};
	});
}

//Archive Course Stretch Goal
/*
module.exports.archiveCourse = (reqParams,reqBody) =>{

	reqBody.isActive = false;
	return Course.findByIdAndUpdate(reqParams.courseId, reqBody).then((course,error)=>{

		if(error){
			return false;
		}else{
			console.log(reqBody)
			return true;
		};
	});
}
*/