const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({

	name : {
		type: String,
		requires: [true,"Course is required!"]
	},
	description: {
		type: String,
		requires: [true,"Description is required!"]
	},
	price : {
		type: Number,
		requires: [true,"Price is required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		//"new Date()" expressions instantiates a new "date" that stores the current date and time whenever a course is created in our db
		default: new Date()
	},
	enrollees: [
		{
			userId : {
				type: String,
				requires: [true,"UserId is required!"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})


module.exports = mongoose.model("Course",courseSchema);